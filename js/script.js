let tabs = document.querySelectorAll(".tabs-title");

let tabContents = document.querySelectorAll(".tabs-content li");

tabs.forEach(function (tab, index) {
  tab.addEventListener("click", function () {
    tabs.forEach(function (tab) {
      tab.classList.remove("active");
    });

    this.classList.add("active");

    tabContents.forEach(function (content) {
      content.style.display = "none";
    });

    tabContents[index].style.display = "block";
  });
});
